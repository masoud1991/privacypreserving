package mokhfa.entities;

import java.io.IOException;

import mokhfa.network.Connection;
import mokhfa.network.Listener;
import mokhfa.network.Server;
import mokhfa.utils.Constants;
import mokhfa.utils.Network;

import com.esotericsoftware.minlog.Log;

public class ServerMachine {

	Server server;
	int clientNumbers = 0;
	int fiMsgNums = 0;

	public ServerMachine() {
		server = new Server() {
			@Override
			protected Connection newConnection() {
				return new MokhfaConnection();
			}
		};
		Network.register(server);
		Log.set(Log.LEVEL_NONE);
		server.addListener(new Listener() {
			@Override
			public void connected(Connection connection) {
				MokhfaConnection mokhfaConnection = (MokhfaConnection) connection;
				Message start = new Message(MessageType.ASSIGN_ID, 0, clientNumbers, null);
				mokhfaConnection.sendTCP(start);
				mokhfaConnection.clientID = clientNumbers;
				clientNumbers++;
				if (clientNumbers == Constants.N) {
					server.sendToAllTCP(new Message(MessageType.START, -1, 0, null));
				}
			}

			@Override
			public void disconnected(Connection connection) {
				super.disconnected(connection);
			}

			@Override
			public void received(Connection connection, Object object) {
				super.received(connection, object);
				if (object instanceof Message) {
					Message msg = (Message) object;
					if (msg.messageType == MessageType.POLYNOMIAL_FI) {
						sendMessage(msg.receiverID, msg);
						fiMsgNums++;
						if (fiMsgNums == Constants.N * Constants.C) {
							try {
								Thread.sleep(100);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
							server.sendToAllTCP(new Message(MessageType.START_SEND_PI, 0, -1, null));
						}
					} else if (msg.messageType == MessageType.POLYNOMIAL_LAMBDA) {
						sendMessage(msg.receiverID, msg);
					} else if (msg.messageType == MessageType.POLYNOMIAL_LAMBDA_N) {
						server.sendToAllTCP(msg);
					}
				}
			}
		});
		try {
			server.bind(Network.port);
			server.start();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendMessage(int receiverID, Message message) {
		for (int i = 0; i < server.getConnections().length; i++) {
			MokhfaConnection conn = (MokhfaConnection) server.getConnections()[i];
			if (conn.clientID == receiverID) {
				conn.sendTCP(message);
				return;
			}
		}
	}

	class MokhfaConnection extends Connection {
		int clientID;
	}
}

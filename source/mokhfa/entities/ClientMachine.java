package mokhfa.entities;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

import mokhfa.network.Client;
import mokhfa.network.Connection;
import mokhfa.network.Listener;
import mokhfa.utils.Constants;
import mokhfa.utils.EncryptionTool;
import mokhfa.utils.Network;
import mokhfa.utils.Polynomial;

import com.esotericsoftware.minlog.Log;

public class ClientMachine {
	int k;
	int set[];
	int clientID;
	Polynomial fi;
	Polynomial receivedFi[] = new Polynomial[Constants.C + 1];
	Polynomial phi = null;
	Polynomial lambda = null;
	ArrayList<Polynomial> randomPolys = new ArrayList<Polynomial>();
	private Client client;
	private Polynomial lambda_n;
	private String address = "localhost";

	public ClientMachine(int k, int[] set) {
		this.k = k;
		this.set = set;
		init();
	}

	public ClientMachine(int k) {
		this.k = k;
		this.set = new int[k];

		Random r = new Random(System.nanoTime());
		for (int i = 0; i < k; i++) {
			set[i] = r.nextInt(2 * k);
		}
		init();
	}

	public ClientMachine() {
		k = Integer.parseInt(JOptionPane.showInputDialog("Enter k"));
		String setVals = JOptionPane.showInputDialog("Enter " + k + " integer seprated by ,");
		StringTokenizer tok = new StringTokenizer(setVals, ",");
		set = new int[k];
		int i = 0;
		while (tok.hasMoreTokens())
			set[i++] = Integer.parseInt(tok.nextToken());
		address = JOptionPane.showInputDialog("Enter server ip address.");
		init();
	}

	private void init() {

		client = new Client();
		client.start();
		Log.set(Log.LEVEL_NONE);
		Network.register(client);
		client.addListener(new Listener() {

			@Override
			public void connected(Connection connection) {
			}

			@Override
			public void disconnected(Connection connection) {
			}

			@Override
			public void received(Connection connection, Object object) {
				if (object instanceof Message) {
					Message msg = (Message) object;
					if (msg.content != null)
						msg.content = EncryptionTool.decryption(msg.content);
					if (msg.messageType == MessageType.ASSIGN_ID) {
						clientID = msg.receiverID;
					} else if (msg.messageType == MessageType.START) {
						for (int i = 1; i <= Constants.C; i++)
							connection.sendTCP(new Message(MessageType.POLYNOMIAL_FI, clientID, (clientID + i)
									% Constants.N, fi.encode()));
					} else if (msg.messageType == MessageType.POLYNOMIAL_FI) {
						receivedFi[(clientID - msg.senderID + Constants.N) % Constants.N] = new Polynomial(msg.content);
					} else if (msg.messageType == MessageType.START_SEND_PI) {
						phi = new Polynomial(0, 0);
						for (int i = 0; i < Constants.C + 1; i++)
							phi = phi.add(receivedFi[i].multiply(randomPolys.get(i)));
						if (clientID == 0) {
							lambda = phi;
							connection.sendTCP(new Message(MessageType.POLYNOMIAL_LAMBDA, clientID, clientID + 1,
									EncryptionTool.encryption(phi.encode())));
						}
					} else if (msg.messageType == MessageType.POLYNOMIAL_LAMBDA) {
						if (clientID == 0) {
							Polynomial lambda_n = new Polynomial(msg.content);
							connection.sendTCP(new Message(MessageType.POLYNOMIAL_LAMBDA_N, clientID, 0, EncryptionTool
									.encryption(lambda_n.encode())));
						} else {
							Polynomial lambda_minus_one = new Polynomial(msg.content);
							lambda = lambda_minus_one.add(phi);
							connection.sendTCP(new Message(MessageType.POLYNOMIAL_LAMBDA, clientID, (clientID + 1)
									% Constants.N, EncryptionTool.encryption(lambda.encode())));
						}
					} else if (msg.messageType == MessageType.POLYNOMIAL_LAMBDA_N) {
						lambda_n = new Polynomial(msg.content);
						printData();
						checkResult();
					}

				}
			}
		});
		fi = fi();
		receivedFi[0] = fi;
		randomPolys = getRandomPolynomials();

		new Thread("Connect") {
			public void run() {
				try {
					client.connect(5000, address, Network.port);
				} catch (IOException ex) {
					ex.printStackTrace();
					System.exit(1);
				}
			}
		}.start();
	}

	private Polynomial fi() {
		// a * x^b
		Polynomial x = new Polynomial(1, 1);// 1*x^1
		Polynomial result = new Polynomial(1, 0);
		for (int i = 0; i < k; i++) {
			result = result.multiply(x.subtract(new Polynomial(set[i], 0)));
		}
		return result;
	}

	private ArrayList<Polynomial> getRandomPolynomials() {
		Random r = new Random(System.nanoTime());
		ArrayList<Polynomial> polys = new ArrayList<Polynomial>();
		for (int i = 0; i < Constants.C + 1; i++) {
			Polynomial p = new Polynomial(0, 0);
			for (int j = 0; j <= k; j++) {
				p = p.add(new Polynomial(r.nextInt(Constants.MAX_RANDOM_POLYNOMIAL_COFFICIENT), j));
			}
			polys.add(p);
		}
		return polys;
	}

	private void checkResult() {
		String res = "ClientId:" + clientID + " : ";
		for (int i = 0; i < k; i++) {
			int a = set[i];
			Polynomial zarib = new Polynomial(1);
			zarib.setCoefficient(0, -a);
			zarib.setCoefficient(1, 1);

			Polynomial temp = new Polynomial(1);
			temp.setCoefficient(0, 1);
			for (int b = 1; b <= Constants.N; b++) {
				temp = temp.multiply(zarib);
				if (lambda_n.remainder(temp).toString().length() != 0) {
					res += "(" + a + "," + (b - 1) + ") , ";
					break;
				}
			}
		}
		System.out.println(res);
		JOptionPane.showMessageDialog(null, res);
	}

	private void printData() {
		String res = "[\n" + "ClientID:" + clientID + "\n";
		for (int i = 0; i < k; i++)
			res += set[i] + ",";
		res += "\n";
		res += "Fi: " + fi.toString();
		res += "\n";
		for (int i = 0; i < Constants.C + 1; i++)
			res += "i:" + randomPolys.get(i).toString() + "\n";
		res += "\n";
		res += "phi: " + phi.toString() + "\n";
		res += "lambda: " + lambda.toString() + "\n";
		res += "lambdaN: " + lambda_n.toString() + "\n";

		res += "]\n";
		System.out.println(res);
		
		

	}

}

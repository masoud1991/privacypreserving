package mokhfa.entities;

import java.io.Serializable;

public class Message implements Serializable{
	private static final long serialVersionUID = 1L;
	MessageType messageType;
	int senderID;
	int receiverID;
	String content;
	public Message(MessageType messageType, int senderID, int receiverID,
			String content) {
		this.messageType = messageType;
		this.senderID = senderID;
		this.receiverID = receiverID;
		this.content = content;
	}
	public Message() {
	}
	
}

package mokhfa.utils;

import mokhfa.entities.Message;
import mokhfa.entities.MessageType;
import mokhfa.network.EndPoint;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.minlog.Log;

// This class is a convenient place to keep things common to both the client and server.
public class Network {
	static public final int port = 54555;

	// This registers objects that are going to be sent over the network.
	static public void register(EndPoint endPoint) {
		Kryo kryo = endPoint.getKryo();
		kryo.register(Message.class);
		kryo.register(MessageType.class);
		kryo.register(PolynomialOld.class);
		Log.set(Log.LEVEL_NONE);
		
	}

}

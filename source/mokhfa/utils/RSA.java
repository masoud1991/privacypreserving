package mokhfa.utils;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;

import javax.crypto.Cipher;

public class RSA {
	
	private KeyPairGenerator keygen;
	private KeyPair keyPair;
	
	public int keySize = 1024;
	
	public RSA() throws NoSuchAlgorithmException {
		keygen = KeyPairGenerator.getInstance("RSA");
		keygen.initialize(keySize);
		keyPair = keygen.generateKeyPair();
	}
	
	public RSA(int keySize) throws NoSuchAlgorithmException{
		keygen = KeyPairGenerator.getInstance("RSA");
		keygen.initialize(keySize);
		this.keySize = keySize;
	}
	
	public String encrypt(String plaintext)  throws Exception
	{
		PublicKey key = keyPair.getPublic();
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

		cipher.init(Cipher.ENCRYPT_MODE, key);

		byte[] ciphertext = cipher.doFinal(plaintext.getBytes("UTF8"));
		return new String(ciphertext, "UTF8");
	}

	public String decrypt(String ciphertext)  throws Exception
	{
		PrivateKey key = keyPair.getPrivate();
		Cipher cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding");

		cipher.init(Cipher.DECRYPT_MODE, key);

		byte[] plaintext = cipher.doFinal(ciphertext.getBytes("UTF8"));
		return new String(plaintext, "UTF8");
	}
	
}
